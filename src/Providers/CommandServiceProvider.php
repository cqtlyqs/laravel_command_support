<?php

namespace Ktnw\CommandSupport\Providers;

use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes($this->getPublishFiles());
    }

    private function getPublishFiles(): array
    {
        $data       = $this->publishData();
        $srcData    = array_column($data, "src");
        $targetData = array_column($data, "target");
        $publishes  = [];
        for ($i = 0; $i < count($srcData); $i++) {
            $publishes[$srcData[$i]] = $targetData[$i];
        }
        return $publishes;
    }

    private function publishData(): array
    {
        return [
            ['src' => __DIR__ . '/../Console/Commands', 'target' => app_path('Console/Commands')],
        ];
    }


}