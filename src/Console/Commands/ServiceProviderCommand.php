<?php

namespace App\Console\Commands;


use Illuminate\Console\GeneratorCommand;

class ServiceProviderCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:serviceProvider {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '绑定业务层service';

    /**
     * 获取生成器的存根文件
     *
     * @return string
     */

    protected function getStub(): string
    {
        return __DIR__ . '/Stubs/serviceProvider.stub';
    }

    /**
     * 获取类的默认命名空间
     *
     * @param  string  $rootNamespace
     * @return string
     */

    protected function getDefaultNamespace($rootNamespace): string
    {
        return $rootNamespace . '\Providers';
    }

}
