<?php

namespace App\Console\Commands;


use Illuminate\Console\GeneratorCommand;

class MakeServiceImpl extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:serviceImpl'; // 重点需要注意的地方，之前是$signature这里记得改下 改成$name

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Create a new serviceImpl class';

    /**
     * 生成类的类型
     *
     * @var string
     */

    protected $type = 'ServicesImpl';

    /**
     * 生产模板文件
     */
    public function handle()
    {
        parent::handle();
        $serviceName    = $this->argument("name");
        $dummyInterface = str_replace("Impl", "", $serviceName);
        $filePath       = app_path("Services/Impl/{$serviceName}.php");
        file_put_contents($filePath, str_replace(["DummyInterface"], [$dummyInterface], file_get_contents($filePath)));
    }

    /**
     * 获取生成器的存根文件
     *
     * @return string
     */

    protected function getStub(): string
    {
        return __DIR__ . '/Stubs/serviceImpl.stub';
    }

    /**
     * 获取类的默认命名空间
     *
     * @param string $rootNamespace
     * @return string
     */

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Services\Impl';
    }


}
