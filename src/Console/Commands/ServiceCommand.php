<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ServiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建service类';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    public function fire()
    {
        $this->call('make:serviceInterface', [
            'name' => $this->argument('name'),
        ]);
        $this->call('make:serviceImpl', [
            'name' => $this->argument('name') . 'Impl',
        ]);
        $this->call('make:serviceBindings', [
            'name' => $this->argument('name'),
        ]);
    }


}
