# laravel_command_support

#### 介绍
常用的laravel的command命令集合

#### 实现command说明
1. 生成Service层: php artisan make:service xxx <br>

#### 使用说明
step1. 引入
```
   composer require ktnw/command_support
```
step2. 发布
```
   php artisan vendor:publish --provider="Ktnw\CommandSupport\Providers\CommandServiceProvider"
```
step3. 修改注释 <br>
   找到AppServiceProvider的register()，在其中增加注释 //register
<br>
step4. 生成service层类
```
   php artisan make:service MyService
```
   运行后，会生成三个文件: <br>
   1) 会在app\Services目录生成接口: MyService <br>
   2) app\Services\Impl目录，生成接口的实现类: MyServiceImpl <br>
   3) 第一次运行php artisan make:service命令，会在app\Providers目录，生成: BusinessServiceProvider。
      且BusinessServiceProvider会在AppServiceProvider中的register()中注册。
      若AppServiceProvider中，未注册。请核实，是否操作了step3。<br>
   
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
